class Hangman
  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  attr_accessor :guesser, :referee, :board, :wrong_count

  def setup
    @wrong_count = 0
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = "_" * word_length
  end

  def take_turn
    if @wrong_count >= 6
      gallows(@wrong_count)
      return false
    elsif @board.include?("_") == false
      puts "Player wins!"
      return false
    end

    choice = @guesser.guess(@board)
    valid_indices = @referee.check_guess(choice)
    update_board(choice, valid_indices)
    @guesser.handle_response(choice, valid_indices)
  end

  def update_board(choice, valid_indices)
    board_check = @board.dup
    valid_indices.each do |index|
      @board[index] = choice
    end

    if board_check == @board
      @wrong_count += 1
      gallows(wrong_count)
    end
  end

  def gallows(wrong_guesses)
    if wrong_guesses == 0

    elsif wrong_guesses == 1

    elsif wrong_guesses == 2

    elsif wrong_guesses == 3

    elsif wrong_guesses == 4

    elsif wrong_guesses == 5

    else

    end
  end
end

class HumanPlayer
  def initialize
    @used_letters = []
  end

  attr_accessor :used_letters

  def guess(board)
    puts board
    valid_choice = false
    until valid_choice
      letter = gets.chomp
      if @used_letters.include?(letter) || !("a".."z").include?(letter.downcase)
        puts "Please choose a letter other than #{@used_letters}"
      else
        valid_choice = true
      end
    end
    letter
  end

  def check_guess(letter)
    puts "Player guessed the following: #{letter}"
    puts "Please indicate which indices (if any) the letter appears in."
    puts "Enter like so: 1st_index_with_letter 2nd_index_with_letter..."
    puts "If guessed letter is incorrect press enter to continue"
    correct_indices = gets.chomp
    correct_indices.split.map(&:to_i)
  end

  def pick_secret_word
    puts "Please enter how many letters your word of choice has:"
    response = gets.chomp
    response.to_i
  end

  def register_secret_length(word_length)
    puts "The chosen word is #{word_length} letters long"
  end

  def handle_response(letter, _indices)
    @used_letters << letter
    puts "Used letters: #{@used_letters}"
  end
end

class ComputerPlayer
  def initialize(word_list = File.readlines("dictionary.txt"))
    @dictionary = word_list.map(&:chomp)
    @secret_word = @dictionary.shuffle[0]
  end

  attr_reader :dictionary, :secret_word

  def pick_secret_word
    @secret_word.length
  end

  def check_guess(letter)
    letter_indices = []
    @secret_word.each_char.with_index do |let, idx|
      letter_indices << idx if let == letter
    end
    letter_indices
  end

  def register_secret_length(word_length)
    @dictionary.select! { |word| word.length == word_length }
  end

  def guess(board)
    letter_counts = Hash.new(0)
    @dictionary.each do |word|
      word.each_char { |ch| letter_counts[ch] += 1 unless board.include?(ch) }
    end

    letter_counts.max_by { |_let, tally| tally }[0]
  end

  def handle_response(letter, indices)
    if indices.empty?
      @dictionary.reject! { |word| word.include?(letter) }
    else
      indices.each do |i|
        @dictionary.select! do |word|
          word[i] == letter && word.count(letter) == indices.length
        end
      end
    end
  end

  def candidate_words
    @dictionary
  end
end
